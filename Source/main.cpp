//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
int factorial (int userNum);

int main (int argc, const char* argv[])
{

    // declarations
    int userNum;
    int result;
    
    std::cout << "Insert the number you would like to count to.\n";
    //assigns user selected int to 'userNum'
    std::cin >> userNum;
    
   
    
    //for loop counts to the number that the user selected
    result = factorial(userNum);
    
    std::cout << "Factorial = " << result << std::endl;
    
    return 0;
}

int factorial (int userNum)
{
    
    int i;
    int result;
    
    
    if (userNum > 0)
    {
        for (i = (userNum - 1); i > 0; i--)
        {
            userNum = (userNum * i);
            
            
        }
        std::cout << "Factorial = " << userNum << std::endl;
        return userNum;
        
    }
    else if (userNum < 0)
    {
        std::cout << "Number must be positive" << std::endl;
    }
    
    return 0;
}